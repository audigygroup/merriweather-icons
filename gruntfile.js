'use strict';
module.exports = function(grunt) {

    grunt.initConfig({
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'gruntfile.js'
            ]
        },
        less: {
            style: {
                options: {
                    cleancss: true,
                },
                files: {
                    'css/enterprise.min.css': 'less/enterprise-fonts.less'
                }
            }
        },
        version: {
            options: {
                css: 'css/enterprise.min.css'
            }
        },
        watch: {
            less: {
                files: [
                    'less/*.less'
                ],
                tasks: ['less']
            }
        },
        clean: {
            dist: [
                'css/enterprise.min.css'
            ]
        }
    });

    // Load tasks
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Register tasks
    grunt.registerTask('default', [
        'clean',
        'less'
    ]);
    grunt.registerTask('dev', [
        'watch'
    ]);

};